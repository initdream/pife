#include <stdlib.h>

typedef struct jogo {
	int estado; // 0 = JOGANDO; 1 = FINALIZADO;
	Jogador** jogadores;
	int jogador_vez;
	Carta* carta_topo;
} Jogo;

Jogo* cria_jogo(Jogador** jogadores, Baralho* baralho) {
	printf("Iniciando o jogo...\n");
		
	Jogo* temp_jogo = (Jogo*) malloc(sizeof(Jogo));
	temp_jogo->estado = 0;
	temp_jogo->jogadores = jogadores;
	int jogador_vez = 0;
	temp_jogo->carta_topo = NULL;
}

void distribuir_cartas(Baralho* baralho, Jogador** jogadores, int tam_arr) {
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < tam_arr; j++) {
			memcpy(jogadores[j]->cartas[i], pegar_carta(baralho), sizeof(Carta));
		}
	}
}

void virar_carta(Jogo* jogo, Baralho* baralho) {
	Carta* carta_para_virar = pegar_carta(baralho);
	jogo->carta_topo = carta_para_virar;
}

void trocar_jogador(Jogo* jogo) {
	// TODO: corigir
	if (jogo->jogador_vez == 1) {
		jogo->jogador_vez = 0;
	} else {
		jogo->jogador_vez++;
	}
}

void trocar_carta(Jogo* jogo, Jogador* jogador, int index_carta_antiga) {
	Carta* carta_antiga = jogador->cartas[index_carta_antiga];
	jogador->cartas[index_carta_antiga] = jogo->carta_topo;
	jogo->carta_topo = carta_antiga;
	trocar_jogador(jogo);
}