typedef struct jogador {
	char nome[20];
	Carta* cartas[9];
} Jogador;

Jogador* cria_jogador(char nome[20]) {
	printf("Criando jogador %s...\n", nome);
	
	Jogador* temp_jogador = (Jogador*) malloc(sizeof(Jogador));
	strcpy(temp_jogador->nome, nome);
	return temp_jogador;
}

void printa_jogador(Jogador* jogador) {
	printf("Printando cartas do jogador %s:\n", jogador->nome);

	for (int i = 0; i < 9; i++) {
		Carta* temp_aux = jogador->cartas[i];
		printa_carta(jogador->cartas[i]);
		free(temp_aux);
	}
}
