#include <stdio.h>
#include "headers.h"

int main () {
	char *locale;
    locale = setlocale(LC_ALL, "");
	printf("Iniciando jogo de pife...\n");

	Baralho* baralho = cria_baralho();
	// printa_baralho(baralho);
	embaralha(baralho);
	// printa_baralho(baralho);

	Jogador* jogador_1 = cria_jogador("Pedro");
	Jogador* jogador_2 = cria_jogador("Bolivar");
	Jogador* jogadores[2] = {jogador_1, jogador_2}; 

	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 2; j++) {
			jogadores[j]->cartas[i] = pegar_carta(baralho);
		}
	}
	Jogo* jogo = cria_jogo(jogadores, baralho);
	char* x = malloc(sizeof(char));
	int aux, temp;
	while(jogo->estado != 1)
	{
		printf("\nQuem está jogando agora é o jogador %s\n", jogo->jogadores[jogo->jogador_vez]->nome);
		
		printf("\nCartas restantes no maço: %d\n", baralho->indice + 1);
		printf("Carta no topo do resto: ");
		if (jogo->carta_topo != NULL) {
			printa_carta(jogo->carta_topo);
		} else {
			printf("NENHUMA");
		}
		printf("\n\n");

		printf("Comandos disponiveis:\nVer meu baralho: v\nMostrar cartas baixadas dos outros jogadores: e\nVirar nova carta: f\nPegar a carta do topo do resto: g\nBater: b\n");
		x[0] = '\0';

		scanf("%s", x);
		if(x[0]!='\0')
		{
			if(x[0]=='v')
			{
				for (aux = 0; aux<9; aux++)
				{
					printa_carta(jogo->jogadores[jogo->jogador_vez]->cartas[aux]);
				}
			}
			else if(x[0]=='e')
			{

			}
			else if(x[0]=='f')
			{
				virar_carta(jogo, baralho);

				printf("Carta no topo do resto: ");
				if (jogo->carta_topo != NULL) {
					printa_carta(jogo->carta_topo);
				} else {
					printf("NENHUMA");
				}
				printf("\n\n");

				printf("Opção:\nPegar carta: s\nPassar: n\n");
				x[0] = '\n';
				scanf("%s", x);

				if(x[0] == 's') {
					int index_carta_antiga;
					for (aux = 0; aux<9; aux++)
					{
						printa_carta(jogo->jogadores[jogo->jogador_vez]->cartas[aux]);
					}
					printf("Carta para ser trocada: ");
					scanf("%d", &index_carta_antiga);
					trocar_carta(jogo, jogo->jogadores[jogo->jogador_vez], index_carta_antiga);
				} else {
					trocar_jogador(jogo);
				}

			}
			else if(x[0]=='g')
			{

			}
			else if(x[0]=='b')
			{

			}
			else
			{
				printf("!!! Comando invalido !!!\n");
			}
		}	
		else
		{
			printf("!!! Comando invalido !!!\n");
		}
		// printf("%s\n", x);
	}
	// for (int i = 0; i < 2; i++) {
	// 	for (int j = 0; j < 9; j++) {
	// 		printa_carta(jogadores[i]->cartas[j]);
	// 	}
	// }
	// distribuir_cartas(baralho, jogadores, 2);	

	printf("Printando indice: %d\n", baralho->indice);

	return 0;
}

