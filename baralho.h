#include <stdlib.h>
#include <time.h>
#include "carta.h"

typedef struct baralho {
	Carta* cartas[104];
	int indice;
} Baralho;

char int_conv(int a)
{
    //converte UM unico digito pra char em ascii
    return a + 48;
}

Baralho* cria_baralho()
{
	printf("Criando baralho...\n");

    //i, j, k são variaveis de controle
    //o vetor frag faz o split de inteiro pra dois caracteres
    Baralho* baralho = (Baralho*) malloc(sizeof(Baralho));
    baralho->indice = 103;

    int i = 0, j=12, k=0;
    for(i; i<104; i++)
    {
        char frag[2];
        for (int m = 0; m<2; m++)
        {
            int aux = 0;
            if (j < 10 && m == 1)
            {
                aux = j;
            }
            if (j > 9)
            {
                //logica pra achar o primeiro e o ultimo digito de cada numero
                if (m == 0) {
                    aux = (int) j/10;
                } else {
                    aux = (int) j % 10;
                }
            }
            frag[m] = int_conv(aux);
        }
		Carta* temp_carta = cria_carta(k, frag);
        baralho->cartas[i] = temp_carta;
        j--;
        if(i == 25 || i == 51 || i == 78)
        {
            k++;
        }
        if(j == -1)
        {
            j=12;
        }
    }

    return baralho;
}

void printa_baralho(Baralho* baralho) {
	printf("Printando baralho...\n");
	
	for (int i = 0; i < 104; i++) {
		printa_carta(baralho->cartas[i]);
	}
}

void embaralha(Baralho* baralho)
{
    printf("Embaralhando...\n");

    srand(time(NULL));

    for (int i = 0; i < 104; i++) {
        int rand_index = rand() % 104;
        Carta* temp_carta = baralho->cartas[rand_index];
        baralho->cartas[rand_index] = baralho->cartas[i];
        baralho->cartas[i] = temp_carta;
    }
}

Carta* pegar_carta(Baralho* baralho) {
	if (baralho->indice < 0) return NULL;
	Carta* temp_carta = baralho->cartas[baralho->indice];
	baralho->indice--;
	return temp_carta;
}