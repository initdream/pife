#include <stdlib.h>
#include <string.h>
#include <locale.h>


typedef struct carta {
	int naipe;
	char valor[2];
	int baixa;
} Carta;

Carta* cria_carta(int naipe, char valor[2]) {
	Carta* aux = (Carta *) malloc(sizeof(Carta));
	aux->naipe = naipe;
	strcpy(aux->valor, valor);
	aux->baixa = 0;
	return aux;
}

void printa_carta(Carta *carta) {
	if(carta->naipe == 0)
	{
		printf("\[♣ ");
	}
	else if(carta->naipe == 1)
	{
		printf("\[♠ ");
	}
	else if(carta->naipe == 2)
	{
		printf("\[♦ ");
	}
	else if(carta->naipe == 3)
	{
		printf("\[♥ ");
	}
	printf("%s] - ", carta->valor);
}
